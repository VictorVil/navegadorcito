<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Usuario::class, function (Faker\Generator $faker) {

    return [
        'rut'=> $faker->unique()->randomNumber($nbDigits = NULL, $strict = false),
        'nombre' => $faker->name,
        'apellido_paterno' => $faker->lastName,
        'apellido_materno' => $faker->lastName,
        'password' => $faker->password,
        'direccion' => $faker->address,
        'mail' => $faker->unique()->email,
        'telefono' => $faker->randomNumber($nbDigits = NULL, $strict = false)
    ];
});


$factory->define(App\RolUsuario::class, function (Faker\Generator $faker) {
     $user_ids = \DB::table('usuarios')->select('rut')->get();
    $user_id = $faker->randomElement($user_ids)->id;

    return [
      'Usuario_rut' => $user_id,
        'Rol_idRol' => $faker->numberBetween($min = 2, $max = 3)
        ];
});
