<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory(App\Usuario::class, 100)->create()->each(function ($u) {
        $u->posts()->save(factory(App\Post::class)->make());
    });

         factory(App\RolUsuario::class, 50)->create()->each(function ($u) {
        $u->posts()->save(factory(App\Post::class)->make());
    });

    }
}
