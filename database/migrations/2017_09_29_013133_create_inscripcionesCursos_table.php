<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscripcionesCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcionesCursos', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('idInscripcionCurso');

            $table->integer('agno');

            $table->integer('semestre');

            $table->string('estado',15);

            $table->integer('InstanciaCurso_idInstanciaCurso');

            $table->foreign('InstanciaCurso_idInstanciaCurso')->references('idInstanciaCurso')->on('instanciasCursos');

            $table->integer('EstadoInscripcion_idEstadoInscripcion');
            $table->foreign('EstadoInscripcion_idEstadoInscripcion')->references('idEstadoInscripcion')->on('estadosInscripciones');

            $table->integer('RolUsuario_Usuario_rut');

            $table->foreign('RolUsuario_Usuario_rut')->references('Usuario_rut')->on('rolesUsuarios');

            $table->integer('RolUsuario_Rol_idRol');

            $table->foreign('RolUsuario_Rol_idRol')->references('Rol_idRol')->on('rolesUsuarios');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcionesCursos');
    }
}
