<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstanciasCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instanciasCursos', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('idInstanciaCurso');

            $table->integer('agno');

            $table->integer('semestre');

            $table->string('Curso_sigla');

            $table->foreign('Curso_sigla')->references('sigla')->on('cursos');

            $table->integer('RolUsuario_Usuario_rut');

            $table->foreign('RolUsuario_Usuario_rut')->references('Usuario_rut')->on('rolesUsuarios');

            $table->integer('RolUsuario_Rol_idRol');

            $table->foreign('RolUsuario_Rol_idRol')->references('Rol_idRol')->on('rolesUsuarios');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instanciasCursos');
    }
}
