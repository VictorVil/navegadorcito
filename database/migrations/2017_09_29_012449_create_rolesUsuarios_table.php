<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rolesUsuarios', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->integer('Usuario_rut')->unsigned();

            $table->foreign('Usuario_rut')->references('rut')->on('usuarios');

            $table->integer('Rol_idRol')->unsigned();

            $table->foreign('Rol_idRol')->references('idRol')->on('roles');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rolesUsuarios');
    }
}
