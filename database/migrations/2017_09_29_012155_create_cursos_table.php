<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->string('sigla',15)->unique();

            $table->primary('sigla');

            $table->string('nombre',45);

            $table->string('descripcion',60);

            $table->integer('MallaCurricular_idMallaCurricular')->unsigned();

            $table->foreign('MallaCurricular_idMallaCurricular')->references('idMallaCurricular')->on('mallasCurriculares');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
