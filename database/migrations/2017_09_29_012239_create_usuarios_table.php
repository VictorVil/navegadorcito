<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {

            $table->engine = 'InnoDB';
	 
            $table->integer('rut');

            $table->primary('rut');

            $table->string('nombre',45);

            $table->string('apellido_paterno',45);

            $table->string('apellido_materno',45);

            $table->string('password',45);

            $table->string('direccion',200);

            $table->string('mail',45);

            $table->integer('telefono');

            $table->integer('telefono2')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
