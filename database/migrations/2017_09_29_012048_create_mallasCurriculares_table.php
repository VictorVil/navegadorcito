<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMallasCurricularesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mallasCurriculares', function (Blueprint $table) {
            $table->increments('idMallaCurricular');

            $table->string('descripcion',150);

            $table->integer('Carrera_idCarrera');

            $table->foreign('Carrera_idCarrera')->references('idCarrera')->on('carreras');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mallasCurriculares');
    }
}
