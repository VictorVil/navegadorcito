<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMallasCurricularesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mallasCurricularesUsuarios', function (Blueprint $table) {

            $table->integer('Usuario_rut');

            $table->foreign('Usuario_rut')->references('rut')->on('usuarios');

            $table->integer('MallaCurricular_idMallaCurricular');

            $table->foreign('MallaCurricular_idMallaCurricular')->references('idMallaCurricular')->on('mallasCurriculares');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mallasCurricularesUsuarios');
    }
}
