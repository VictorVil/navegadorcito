<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/***Rutas para Admin (Jorge)***/

//Home
Route::name('home_admin_path')->get('/administrador','AdminController@homeAdmin');

//Mantenedor de Profesores
Route::name('mantenedor_profesores_path')->get('/administrador/mantenedor_profesores', 'ProfesorController@mantenedorProfesores');
Route::name('crear_profesor_path')->get('/administrador/mantenedor_profesores/nuevo','ProfesorController@crearProfesor');
Route::name('crear_profesor_path')->post('/administrador/mantenedor_profesores/nuevo', 'ProfesorController@guardarProfesor');
Route::name('editar_profesor_path')->get('/administrador/mantenedor_profesores/{rut_profesor}','ProfesorController@editarProfesor');
Route::name('editar_profesor_path')->post('/administrador/mantenedor_profesores/{rut_profesor}', 'ProfesorController@actualizarProfesor');

//Mantenedor de Alumnos
Route::name('mantenedor_alumnos_path')->get('/administrador/mantenedor_alumnos', 'AlumnoController@mantenedorAlumnos');
Route::name('crear_alumno_path')->get('/administrador/mantenedor_alumnos/nuevo','AlumnoController@crearAlumno');
Route::name('crear_alumno_path')->post('/administrador/mantenedor_alumnos/nuevo', 'AlumnoController@guardarAlumno');
Route::name('editar_alumno_path')->get('/administrador/mantenedor_alumnos/{rut_alumno}','AlumnoController@editarAlumno');
Route::name('editar_alumno_path')->post('/administrador/mantenedor_alumnos/{rut_alumno}', 'AlumnoController@actualizarAlumno');

//Mantenedor de

/***Rutas para Profesor (Victor)***/


/***Rutas para Alumno (Maicholl)***/



