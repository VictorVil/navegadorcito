<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="="UTF-8">
@yield('title_html')
<!-- <title>Backend Administrativo [MedMorandum]</title> -->

<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->

<style>
	table {
	    font-family: arial, sans-serif;
	    border-collapse: collapse;
	    width: 100%;
	    align: center;

	}

	td, th {
	    border: 1px solid #dddddd;
	    text-align: left;
	    padding: 8px;
	}

	tr:nth-child(even) {
	    background-color: #dddddd;
	}
	</style>

</head>
<body>
<div class="container">
	<div id="titulo">
		@yield('titulo')
	</div>
	<hr>
	@yield('content')
</div>
</body>
