@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
	        <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
                    <div class="panel-heading">Mantenedor de Profesores</div>

                    <div class="panel-body">

					    <input type="button" class="btn btn-primary" value="Volver" onclick="window.location.href='{{ route('home_admin_path') }}';">
						<input type="button" class="btn btn-primary" value="Agregar Profesor" onclick="window.location.href='{{ route('crear_profesor_path') }}';">
					</div>

                    <div class="panel-body">
                    	<table border="1">

                    		<thead>
								<tr>
									<td>RUT </td>
									<td>Nombre </td>
									<td>Correo </td>
									<td>Teléfono </td>
									<td> </td>
									<!--<td> </td>-->
								</tr>
							</thead>

							<tbody>
								@foreach($profesores as $profesor)
									<tr>
										<td> {{ $profesor->rut }} </td>
										<td> {{ $profesor->nombre }} </td>
										<td> {{ $profesor->mail }} </td>
										<td> {{ $profesor->telefono }} </td>
										<td> <a href="{{ route('editar_profesor_path', ['rut' => $profesor->rut]) }}" class="btn btn-primary">Editar</a> </td>
										<?php /*<td>
											<form action="{{ route('eliminar_paciente_path', ['rut' => $paciente->rut]) }}" method="POST">
												{{ csrf_field() }}
                       							{{ method_field('DELETE') }}

                       							<button type="submit" class="btn btn-primary">Eliminar</button>
											</form>
										</td>*/ ?>
									</tr>
								@endforeach
							</tbody>

                    	</table>
                    </div>
            </div>
        </div>
    </div>
@endsection
