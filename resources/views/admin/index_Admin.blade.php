@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
	        <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
                    <div class="panel-heading">Backend Administrativo Navegadorcito</div>

                    <div class="panel-body">

                       	<div class="form-group">
	                       	<div class="col-md-6">
		                    	<input type="button" class="btn btn-primary" value="Mantenedor de Profesores" onclick="window.location.href='{{ route('mantenedor_profesores_path') }}';">
		                   	</div>
	                   	</div>
	                   	<br>

	                   	<div class="form-group">
		                   	<div class="col-md-6">
		                    	<input type="button" class="btn btn-primary" value="Mantenedor de Alumnos" onclick="window.location.href='{{ route('mantenedor_alumnos_path') }}';">
		                    </div>
		                </div>
		                <br>

	                    <div class="form-group">
		                    <div class="col-md-6">
		                    	<input type="button" class="btn btn-primary" value="Mantenedor de Carreras y Mallas Curriculares" onclick="">
		                    </div>
		                </div>
		                <br>

	                    <div class="form-group">
		                    <div class="col-md-6">
		                    	<input type="button" class="btn btn-primary" value="Mantenedor de Matrículas" onclick="">
		                    </div>
		                </div>
                        <br>
                        <div class="form-group">
		                    <div class="col-md-6">
		                    	<input type="button" class="btn btn-primary" value="Mantenedor de Cursos" onclick="">
		                    </div>
		                </div>
                        <br>
                        <div class="form-group">
		                    <div class="col-md-6">
		                    	<input type="button" class="btn btn-primary" value="Cerrar Sesión" onclick="">
		                    </div>
		                </div>

                    </div>

                </div>
	        </div>
	    </div>
	</div>
@endsection
