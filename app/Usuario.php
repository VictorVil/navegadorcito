<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model {

    protected $table = 'usuarios';

    protected $fillable = ['rut','nombre','apellido_paterno','apellido_materno','password','direccion','mail','telefono','telefono2'];

}
