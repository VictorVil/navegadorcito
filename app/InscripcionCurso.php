<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscripcionCurso extends Model {

    protected $table = 'inscripcionesCursos';

    protected $fillable = ['idInscripcionCurso','agno','semestre','estado','InstanciaCurso_idInstanciaCurso','EstadoInscripcion_idEstadoInscripcion','RolUsuario_Usuario_rut','RolUsuario_Rol_idRol'];

}
