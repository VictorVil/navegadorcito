<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\RolUsuario;
use App\Rol;

class ProfesorController extends Controller
{
	public function mantenedorProfesores() {

        $profesores = RolUsuario::where('Rol_idRol', 3)->get();

        $retornoProfesores = array();

        $contador = 0;

        foreach ($profesores as $profesor) {

            $usuario = Usuario::where('rut', $profesor->Usuario_rut)->first();

            $retornoProfesores[$contador] = $usuario;

            $contador++;

        }

        return view('admin.mantenedor_profesores')->with(['profesores' => $retornoProfesores]);

    }

    public function crearProfesor() {

    	return view('admin.crear_profesor');

    }

    public function guardarProfesor(Request $request){

        $profesor = new Usuario;

        $profesor->rut = $request->get('rut');

        $profesor->nombre = $request->get('nombre');

        $profesor->apellido_paterno = $request->get('apellido_paterno');

        $profesor->apellido_materno = $request->get('apellido_materno');

        $profesor->password = bcrypt($request->get('password'));

        $profesor->direccion = $request->get('direccion');

        $profesor->mail = $request->get('mail');

        $profesor->telefono = $request->get('telefono');

        $profesor->telefono2 = $request->get('telefono2');

        $profesor->save();

        $rolUsuario = New RolUsuario;

        $rolUsuario->Rol_idRol = 3;

        $rolUsuario->Usuario_rut = $request->get('rut');

        $rolUsuario->save();

        return redirect()->route('mantenedor_profesores_path');

    }

    public function editarProfesor($rut) {

        $usuario = Usuario::where('rut', $rut)->first();

        return view('admin.editar_profesor', ['usuario' => $usuario]);

    }

    public function actualizarProfesor($rut, Request $request){

        $profesor = Usuario::where('rut',$rut)->first();

        if($request->get('password') != null){

            $profesor->password = bcrypt($request->get('password'));

            $profesor->save();

        }

        $profesor->update($request->only('nombre','telefono'));

        return redirect()->route('mantenedor_profesores_path');

    }

}
