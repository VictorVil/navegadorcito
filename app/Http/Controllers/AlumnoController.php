<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\RolUsuario;
use App\Rol;

class AlumnoController extends Controller
{
	public function mantenedorAlumnos() {

        $alumnos = RolUsuario::where('Rol_idRol', 2)->get();

        $retornoAlumnos = array();

        $contador = 0;

        foreach ($alumnos as $alumno) {

            $usuario = Usuario::where('rut', $alumno->Usuario_rut)->first();

            $retornoAlumnos[$contador] = $usuario;

            $contador++;

        }

        return view('admin.mantenedor_alumnos')->with(['alumnos' => $retornoAlumnos]);

    }

    public function crearAlumno() {

    	return view('admin.crear_alumno');

    }

    public function guardarAlumno(Request $request){

        $alumno = new Usuario;

        $alumno->rut = $request->get('rut');

        $alumno->nombre = $request->get('nombre');

        $alumno->apellido_paterno = $request->get('apellido_paterno');

        $alumno->apellido_materno = $request->get('apellido_materno');

        $alumno->password = bcrypt($request->get('password'));

        $alumno->direccion = $request->get('direccion');

        $alumno->mail = $request->get('mail');

        $alumno->telefono = $request->get('telefono');

        $alumno->telefono2 = $request->get('telefono2');

        $alumno->save();

        $rolUsuario = New RolUsuario;

        $rolUsuario->Rol_idRol = 2;

        $rolUsuario->Usuario_rut = $request->get('rut');

        $rolUsuario->save();

        return redirect()->route('mantenedor_alumnos_path');

    }

    public function editarAlumno($rut) {

        $usuario = Usuario::where('rut', $rut)->first();

        return view('admin.editar_alumno', ['usuario' => $usuario]);

    }

    public function actualizarAlumno($rut, Request $request){

        $alumno = Usuario::where('rut',$rut)->first();

        if($request->get('password') != null){

            $alumno->password = bcrypt($request->get('password'));

            $alumno->save();

        }

        $alumno->update($request->only('nombre','telefono'));

        return redirect()->route('mantenedor_alumnos_path');

    }

}
