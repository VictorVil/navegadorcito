<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model {

    protected $table = 'cursos';

    protected $fillable = ['sigla','nombre','descripcion','MallaCurricular_idMallaCurricular'];

}
