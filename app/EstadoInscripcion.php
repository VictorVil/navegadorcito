<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoInscripcion extends Model {

    protected $table = 'estadosInscripciones';

    protected $fillable = ['idEstadoInscripcion','nombre'];

}
