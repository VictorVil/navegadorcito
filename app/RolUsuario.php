<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolUsuario extends Model {

    protected $table = 'rolesUsuarios';

    protected $fillasble = ['Usuario_rut','Rol_idRol'];

}
