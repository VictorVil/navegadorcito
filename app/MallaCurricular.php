<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MallaCurricular extends Model {

    protected $table = 'mallasCurriculares';

    protected $fillable = ['idMallaCurricular','descripcion','Carrera_idCarrera'];

}
