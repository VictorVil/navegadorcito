<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MallaCurricularUsuario extends Model {

    protected $table = 'mallasCurricularesUsuarios';

    protected $fillable = ['Usuario_rut','MallaCurricular_idMallaCurricular'];

}
