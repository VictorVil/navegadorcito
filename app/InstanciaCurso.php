<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstanciaCurso extends Model {

    protected $table = 'instanciasCursos';

    protected $fillable = ['idInstanciaCurso','agno','semestre','Curso_sigla','RolUsuario_Usuario_rut','RolUsuario_Rol_idRol'];

}
